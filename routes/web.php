<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', HomeController::class)->name('home');
Route::get('about', AboutController::class)->name('about');
Route::get('delivery', DeliveryController::class)->name('delivery');
Route::get('terms', TermsController::class)->name('terms');
Route::get('search', SearchController::class)->name('search');

Route::resource('products', ProductController::class);
Route::resource('categories', CategoryController::class);
Route::resource('collections', CollectionController::class);

Route::get('quick-product/{product}', 'ProductController@quickShow');

Route::prefix('contact')
    ->group(function ($router) {
        $router->get('/', 'ContactController@index')->name('contact.index');
        $router->post('/', 'ContactController@store')->name('contact.store');
    });

Route::get('cart/content', CartContentController::class)->middleware('check.cart')->name('cart.content');
Route::put('cart/content', 'CartController@updateBulk')->name('cart.content.update');
Route::get('cart/count', 'CartController@count')->name('cart.count');
Route::resource('cart', CartController::class)->only('index', 'store', 'update', 'destroy');

Route::resource('checkout', CheckoutController::class);

Route::namespace('Admin')
    ->prefix('admin')
    ->as('admin.')
    ->middleware(['auth'])
    ->group(function ($router) {
        $router->get('/', HomeController::class);

        $router->resource('collections', CollectionController::class);
        $router->resource('categories', CategoryController::class);
        $router->resource('products', ProductController::class);
        $router->resource('colors', ColorController::class);
        $router->resource('sizes', SizeController::class);
        $router->resource('cloths', ClothController::class);
        $router->resource('products.colors', ProductColorController::class)->only('index', 'store');
        $router->resource('products.sizes', ProductSizeController::class)->only('index', 'store');
        $router->resource('products.cloths', ProductClothController::class)->only('index', 'store');
        $router->resource('products.pictures', ProductPictureController::class)->only('index', 'store', 'destroy');

        $router->resource('orders', OrderController::class);
        $router->resource('orders.details', OrderDetailController::class);

        $router->resource('first-page', FirstPagePictureController::class)->only('index', 'store', 'destroy');

        $router->resource('about', AboutController::class)->only('index', 'store');
        $router->resource('delivery', DeliveryController::class)->only('index', 'store');
        $router->resource('terms', TermsController::class)->only('index', 'store');
    });
