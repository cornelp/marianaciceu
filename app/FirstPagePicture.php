<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstPagePicture extends Model
{
    protected $fillable = [
        'picture_id', 'text', 'title'
    ];

    protected $with = [
        'picture'
    ];

    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }
}
