<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Anam\Phpcart\Cart;
use App\Http\Requests\OrderRequest;
use App\Order;
use Mail;
use App\Mail\ClientOrderMail;
use App\Mail\AdminOrderMail;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.cart')->only('index', 'store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = new Cart;

        return view('checkout.index', compact('cart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        // add order and details
        $order = $this->createOrder($request);

        // send mail
        $this->sendMails($request);

        // clear cart
        $this->clearCart();

        // set flash data
        $request->session()->flash('thank-you', 'Va multumim pentru comanda.');

        // redirect user
        return redirect()->route('home');
    }

    protected function createOrder($request)
    {
        $order = Order::create($request->all());

        (new Cart)->items()->each(function ($item) use ($order) {
            $order->details()->create([
                'product_id' => $item->product->id,
                'quantity' => $item->quantity,
                'size_id' => $item->product->sizes->first()->id,
                'color_id' => $item->product->colors->first()->id,
                'cloth_id' => $item->product->cloths->first()->id,
                'price' => $item->product->locale->pivot->price,
                'text' => 'ceva'
            ]);
        });
    }

    protected function sendMails($request)
    {
        $cart = new Cart;

        Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(new AdminOrderMail($request, $cart));

        Mail::to($request->email)
            ->send(new ClientOrderMail($request, $cart));
    }

    protected function clearCart()
    {
        return (new Cart)->clear();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
