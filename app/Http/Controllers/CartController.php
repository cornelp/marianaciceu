<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Anam\Phpcart\Cart;
use App\Http\Requests\CartRequest;

class CartController extends Controller
{
    protected $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    protected function getId(Product $product)
    {
        return md5(
            $product->id .
            $product->cloths->first()->id .
            $product->colors->first()->id .
            $product->sizes->first()->id
        );
    }

    protected function getProductFromRequest($request)
    {
        $product = Product::with([
            'sizes' => function ($query) use ($request) {
                $query->where('size_id', $request->size);
            },
            'cloths' => function ($query) use ($request) {
                $query->where('cloth_id', $request->cloth);
            },
            'colors' => function ($query) use ($request) {
                $query->where('color_id', $request->color);
            }
        ])
        ->actives()
        ->findOrFail($request->product);

        return $product;
    }

    public function store(CartRequest $request)
    {
        $product = $this->getProductFromRequest($request);

        if (! $product) {
            return $this->renderResponse();
        }

        $this->cart->add([
            'id'       => $this->getId($product),
            'name'     => $product->locale->pivot->name,
            'quantity' => $request->quantity,
            'product'  => $product,
            'price'    => $product->getFinalPrice()
        ]);

        return $this->renderResponse();
    }

    protected function index()
    {
        return $this->renderResponse();
    }

    protected function renderResponse()
    {
        return view('layouts.cart', ['cart' => $this->cart]);
    }

    public function count()
    {
        return ['items' => $this->cart->count()];
    }

    public function update(Request $request)
    {
        $this->handleUpdate($request->only('id', 'quantity'));

        return $this->renderResponse();
    }

    protected function handleUpdate($id, $quantity)
    {
        $quantity > 0
            ? $this->cart->update(compact('id', 'quantity'))
            : $this->cart->remove($id);
    }

    public function updateBulk(Request $request)
    {
        collect($request->items)->each(function ($value, $id) {
            $this->handleUpdate($id, $value);
        });

        return redirect()->route('cart.content');
    }

    public function destroy($id)
    {
        $this->cart->remove($id);

        return $this->renderResponse();
    }
}
