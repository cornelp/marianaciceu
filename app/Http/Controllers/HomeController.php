<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FirstPagePicture;
use App\Product;
use App\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $items = FirstPagePicture::all();
        $newest = Product::with('pictures')
            ->actives()->take(8)->get();

        return view('home', compact('items', 'newest'));
    }
}
