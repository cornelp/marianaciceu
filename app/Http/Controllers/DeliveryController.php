<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    public function __invoke()
    {
        $content = view('delivery.content')->render();

        return view('delivery.index', compact('content'));
    }
}
