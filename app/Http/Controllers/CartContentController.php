<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Anam\Phpcart\Cart;

class CartContentController extends Controller
{
    public function __invoke()
    {
        $cart = new Cart;

        return view('cart-content.index', compact('cart'));
    }
}
