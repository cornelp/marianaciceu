<?php

namespace App\Http\Controllers\Admin;

use App\OrderDetail;
use App\Order;
use App\Product;
use App\Color;
use App\Size;
use App\Cloth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function index(Order $order)
    {
        $order->load('details');

        return view('admin.orders.details.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order, OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order, $id)
    {
        $products = Product::get()->pluck('locale.pivot.name', 'id')->all();
        $colors = Color::all()->pluck('name', 'id')->all();
        $sizes = Size::all()->pluck('name', 'id')->all();
        $cloths = Cloth::all()->pluck('name', 'id')->all();
        $orderDetail = OrderDetail::find($id);

        return view('admin.orders.details.edit',
            compact('orderDetail', 'products', 'colors', 'sizes', 'cloths'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order, $id)
    {
        OrderDetail::findOrFail($id)->update($request->all());

        return redirect()->route('admin.orders.details.index', [$order->id, $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order, OrderDetail $orderDetail)
    {
        //
    }
}
