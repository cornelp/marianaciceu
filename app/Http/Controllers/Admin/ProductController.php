<?php

namespace App\Http\Controllers\Admin;

use App\Locale;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Collection;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::with('category', 'collection')
            ->customSearch($request->search)
            ->orderBy($request->input('sort', 'id'), $request->input('order', 'asc'))
            ->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->pluck('name', 'id')->all();
        $collections = Collection::all()->pluck('name', 'id')->all();

        return view('admin.products.create', compact('categories', 'collections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required|min:1|max:255',
            'category_id' => 'required|numeric',
            'collection_id' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required',
            'is_new' => 'required',
            'is_active' => 'required',
        ]);

        $product = Product::create($validated);
        $locale = Locale::getDefault()->id;

        $product->locales()->attach(
            $locale,
            $request->only('name', 'price', 'description')
        );

        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return redirect()->route('admin.products.edit', $product->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all()->pluck('name', 'id')->all();
        $collections = Collection::all()->pluck('name', 'id')->all();

        $data = array_merge(
            $product->locale->pivot->only('name', 'price', 'description'),
            $product->toArray()
        );

        $data['discount'] *= 100;

        return view('admin.products.edit', compact('data', 'categories', 'collections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validated = $this->validate($request, [
            'name' => 'required|min:1|max:255',
            'category_id' => 'required|numeric',
            'collection_id' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required',
            'is_new' => 'required',
            'is_active' => 'required',
            'discount' => 'numeric',
        ]);

        $product->update($validated);
        $locale = Locale::getDefault();

        $product->locales()->updateExistingPivot($locale->id, $request->only('name', 'price', 'description'));

        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('admin.products.index');
    }
}
