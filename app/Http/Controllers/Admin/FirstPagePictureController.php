<?php

namespace App\Http\Controllers\Admin;

use App\FirstPagePicture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Picture;

class FirstPagePictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = FirstPagePicture::with('picture')->get();

        return view('admin.first-page.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'picture' => 'required|file',
            'alt' => 'required|max:255',
            'title' => 'required',
            'text' => 'required'
        ]);

        $picture = Picture::fromRequest($request);

        FirstPagePicture::create([
            'picture_id' => $picture->id,
            'title' => $request->title,
            'text' => $request->input('text', '-'),
        ]);

        return redirect()->route('admin.first-page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FirstPagePicture  $firstPagePicture
     * @return \Illuminate\Http\Response
     */
    public function show(FirstPagePicture $firstPagePicture)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FirstPagePicture  $firstPagePicture
     * @return \Illuminate\Http\Response
     */
    public function edit(FirstPagePicture $firstPagePicture)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FirstPagePicture  $firstPagePicture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FirstPagePicture $firstPagePicture)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FirstPagePicture  $firstPagePicture
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FirstPagePicture::findOrFail($id)->delete();

        return redirect()->route('admin.first-page.index');
    }
}
