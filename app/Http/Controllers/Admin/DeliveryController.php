<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class DeliveryController extends Controller
{
    public function index()
    {
        $content = view('delivery.content')->render();

        return view('admin.delivery.index', compact('content'));
    }

    public function store(Request $request)
    {
        File::put(resource_path('views/delivery/content.blade.php'), $request->content);

        return redirect()->route('admin.delivery.index');
    }
}
