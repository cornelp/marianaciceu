<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $colors = Color::get();
        $product->load('colors');
        $colorIds = $product->colors->pluck('id')->all();

        return view('admin.products.colors.index', compact('product', 'colors', 'colorIds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $product->colors()->sync($request->color);

        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Color $color)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Color $color)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Color $color)
    {
        //
    }
}
