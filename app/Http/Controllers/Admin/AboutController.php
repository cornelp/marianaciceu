<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class AboutController extends Controller
{
    public function index()
    {
        $content = view('about.content')->render();

        return view('admin.about.index', compact('content'));
    }

    public function store(Request $request)
    {
        File::put(resource_path('views/about/content.blade.php'), $request->content);

        return redirect()->route('admin.about.index');
    }
}
