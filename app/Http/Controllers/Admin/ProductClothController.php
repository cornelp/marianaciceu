<?php

namespace App\Http\Controllers\Admin;

use App\Cloth;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductClothController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $cloths = Cloth::all();
        $product->load('cloths');
        $clothIds = $product->cloths->pluck('id')->all();

        return view('admin.products.cloths.index', compact('product', 'clothIds', 'cloths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $product->cloths()->sync($request->cloth);

        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Size $size)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, Size $size)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Size $size)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Size $size)
    {
        //
    }
}
