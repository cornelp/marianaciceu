<?php

namespace App\Http\Controllers\Admin;

use App\Cloth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClothController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cloths = Cloth::search($request->search)
            ->orderBy($request->input('sort', 'id'), $request->input('order', 'asc'))
            ->paginate(10);

        return view('admin.cloths.index', compact('cloths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cloths.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required|max:255|unique:cloths,name'
        ]);

        Cloth::create($validated);

        return redirect()->route('admin.cloths.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.cloths.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function edit(Cloth $cloth)
    {
        return view('admin.cloths.edit', compact('cloth'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cloth $cloth)
    {
        $validated = $this->validate($request, [
            'name' => 'required|min:1|max:255'
        ]);

        $cloth->update($validated);

        return redirect()->route('admin.cloths.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cloth  $cloth
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cloth $cloth)
    {
        //
    }
}
