<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class TermsController extends Controller
{
    public function index()
    {
        $content = view('terms.content')->render();

        return view('admin.terms.index', compact('content'));
    }

    public function store(Request $request)
    {
        File::put(resource_path('views/terms/content.blade.php'), $request->content);

        return redirect()->route('admin.terms.index');
    }
}
