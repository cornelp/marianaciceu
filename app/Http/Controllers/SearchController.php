<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SearchController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = $request->search;
        $query = Product::with('pictures');

        if ($search) {
            $query->customSearch($search);
        }

        $products = $query->paginate(10);

        return view('search.index', compact('products', 'search'));
    }
}
