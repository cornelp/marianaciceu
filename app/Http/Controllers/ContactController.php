<?php

namespace App\Http\Controllers;

use Mail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function store(ContactRequest $request)
    {
        Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(new ContactMail($request));

        $request->session()->flash('thank-you', 'Multumim! Mesajul tau a fost trimis.');

        return redirect()->route('home');
    }
}
