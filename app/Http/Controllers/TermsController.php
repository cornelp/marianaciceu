<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TermsController extends Controller
{
    public function __invoke()
    {
        $content = view('terms.content')->render();

        return view('terms.index', compact('content'));
    }
}
