<?php

namespace App\Http\Middleware;

use Closure;
use Anam\Phpcart\Cart;

class CheckCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! (new Cart)->count()) {
            $request->session()->flash('cart-error', 'Nu ai niciun produs in cos.');

            return redirect()->route('home');
        }

        return $next($request);
    }
}
