<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'is_new', 'discount', 'is_active',
        'category_id', 'collection_id'
    ];

    protected $casts = [
        'is_new' => 'boolean',
        'is_active' => 'boolean',
    ];

    protected $with = [
        'locales'
    ];

    public function setDiscountAttribute($value)
    {
        $this->attributes['discount'] = $value / 100;
    }

    public function locales()
    {
        return $this->belongsToMany(Locale::class)
            ->withPivot(['name', 'price', 'description']);
    }

    public function colors()
    {
        return $this->belongsToMany(Color::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }

    public function pictures()
    {
        return $this->belongsToMany(Picture::class);
    }

    public function cloths()
    {
        return $this->belongsToMany(Cloth::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    public function scopeByCollection($query, $collectionId)
    {
        $query->whereHas('collection', function ($query) use ($collectionId) {
            $query->where('id', $collectionId);
        });
    }

    public function scopeByCategory($query, $categoryId)
    {
        $query->whereHas('category', function ($query) use ($categoryId) {
            $query->where('id', $categoryId);
        });
    }

    public function scopeCustomSearch($query, $search)
    {
        $query
            ->whereHas('locales', function ($query) use ($search) {
                $query->where('locale_product.name', 'LIKE', '%' . $search . '%')
                    ->orWhere('locale_product.description', 'LIKE', '%' . $search . '%')
                    ->orWhere('locale_product.price', 'LIKE', '%' . $search . '%');
            });
    }

    public function getLocaleAttribute()
    {
        return $this->locales
            ->where('name', app()->getLocale())
            ->first();
    }

    public function getFinalPrice()
    {
        return $this->locale->pivot->price - $this->locale->pivot->price * $this->discount;
    }

    public function getFirstPicture()
    {
        return optional($this->pictures->first());
    }

    public function getDiscount()
    {
        return $this->discount * 100;
    }

    public function scopeActives($query)
    {
        $query->where('is_active', true);
    }
}
