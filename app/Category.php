<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Category extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'name', 'is_active'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $searchable = [
        'columns' => [
            'name' => 10
        ]
    ];

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
