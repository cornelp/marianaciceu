<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    const DEFAULT = 'ro';

    protected $fillable = [
        'name', 'symbol'
    ];

    public static function getDefault()
    {
        return self::where('name', self::DEFAULT)->first();
    }

    public function scopeByName($query, $name)
    {
        $query->where('name', $name);
    }
}
