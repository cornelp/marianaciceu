<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\Collection;
use App\Category;
use Illuminate\Support\Facades\Schema;
use Anam\Phpcart\Cart;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! $this->app->runningInConsole() && ! request()->is('admin/*')) {
            view()->share('categories', Category::active()->get());
            view()->share('collections', Collection::get());
            view()->share('cart', new Cart);
        }

        Blade::component('components.box', 'box');
        Blade::component('components.search', 'search');
        Blade::component('components.add', 'add');
        Blade::component('components.theader', 'theader');

        Blade::directive('float', function ($value) {
            return "<?php echo number_format({$value}, 2); ?>";
        });

        app()->setLocale('ro');

		Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
