<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Size extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'name'
    ];

    protected $searchable = [
        'columns' => [
            'name' => 10
        ]
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function scopeByCategory($query, $categoryId)
    {
        $query->whereHas('products', function ($query) use ($categoryId) {
            $query->whereHas('category', function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            });
        });
    }

    public function scopeByCollection($query, $categoryId)
    {
        $query->whereHas('products', function ($query) use ($categoryId) {
            $query->whereHas('collection', function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            });
        });
    }
}
