<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Requests\OrderRequest;
use Anam\Phpcart\Cart;

class AdminOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $request;
    protected $cart;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OrderRequest $request, Cart $cart)
    {
        $this->request = $request;
        $this->cart = $cart;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.order.admin')
            ->with('request', $this->request)
            ->with('cart', $this->cart);
    }
}
