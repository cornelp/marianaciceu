@extends('layouts.master')

@section('content')
    <div class="slider-area">
        <div class="slider-active owl-carousel">
            @foreach ($items as $item)
                <div class="single-slider slider-1 gray-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="slider-content slider-animated-1">
                                    <h2 class="animated">{{ $item->title }}</h2>
                                    <p class="animated">{{ $item->text }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="slider-single-img slider-animated-1">
                                    <img class="animated" src="{{ asset('images/' . $item->picture->name) }}" alt="{{ $item->picture->alt }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="product-area pb-80">
        <div class="container">

            @if (session()->has('cart-error'))
                <div class="alert alert-danger">{{ session()->get('cart-error') }}</div>
            @endif

            @if (session()->has('thank-you'))
                <div class="alert alert-success">{{ session()->get('thank-you') }}</div>
            @endif
            <div style="height:20px"></div>
            <div class="section-title text-center mb-20">
                <h2>Cele mai noi produse</h2>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="home1" role="tabpanel">
                    <div class="row">
                        @foreach ($newest as $product)
                            <div class="col-md-6 col-lg-4 col-xl-3 col-6">
                                <div class="product-wrapper mb-45">
                                    <div class="product-img">
                                        <a href="{{ route('products.show', [$product->id]) }}">
                                            <img src="{{ asset('images/' . $product->getFirstPicture()->name) }}" alt="{{ $product->getFirstPicture()->alt }}">
                                        </a>
                                        @if ($product->discount)
                                            <span>{{ $product->getDiscount() }}% redus</span>
                                        @endif
                                        <div class="product-action">
                                            <div class="product-action-style">
                                                <a class="action-plus" title="Quick View" data-toggle="modal" data-target="#exampleModal" onclick="getQuickProduct(event, {{ $product->id }})" href="#">
                                                    <i class="ti-plus"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-content text-center">
                                        <h4><a href="{{ route('products.show', [$product->id]) }}">{{ $product->locale->pivot->name }}</a></h4>
                                        <div class="product-price">
                                            @if ($product->discount)
                                                <span class="old">@float($product->locale->pivot->price) {{ $product->locale->symbol }}</span>
                                            @endif
                                            <span>@float($product->getFinalPrice()) {{ $product->locale->symbol }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="banner-area pb-95">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner-wrapper overflow mb-30">
                        <div class="banner-img">
                            <a href="#">
                                <img alt="image" src="assets/img/banner/6.jpg">
                            </a>
                        </div>
                        <div class="banner-content-5">
                            <h4>New Arrivals</h4>
                            <h2>Rattan Sofa</h2>
                            <h3>Sale up to 30% off all</h3>
                            <a href="#" class="banner-btn">View Collection</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="banner-wrapper overflow mb-30">
                        <div class="banner-img">
                            <a href="#">
                                <img alt="image" src="assets/img/banner/7.jpg">
                            </a>
                        </div>
                        <div class="banner-content-5">
                            <h4>Best Products</h4>
                            <h2>Rattan Accessories</h2>
                            <h3>Sale up to 40% off all</h3>
                            <a href="#" class="banner-btn">View Collection</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
@endsection