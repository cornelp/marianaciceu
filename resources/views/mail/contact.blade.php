@component('mail::message')
Buna ziua,

Aveti un nou mesaj de la {{ $request->name }}, adresa de mail {{ $request->email }}.
Puteti vedea continutul ceva mai jos

Subiect:
{{ $request->subject }}

Mesaj:
{{ $request->message }}
@endcomponent