@extends('layouts.master')

@section('content')
<div style="height:250px;"></div>
<div class="contact-area hm-4-padding">
    <div class="all-info ptb-130">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-info-wrapper">
                        <h4 class="contact-title">Informatii</h4>
                        <div class="communication-info">
                            <div class="single-communication">
                                <div class="communication-icon">
                                    <i class="ti-home" aria-hidden="true"></i>
                                </div>
                                <div class="communication-text">
                                    <h4>Adresa:</h4>
                                    <p>Romania, Bacau, Str Banca Nationala</p>
                                </div>
                            </div>
                            <div class="single-communication">
                                <div class="communication-icon">
                                    <i class="ti-mobile" aria-hidden="true"></i>
                                </div>
                                <div class="communication-text">
                                    <h4>Telefon:</h4>
                                    <p>0701 01 01 01</p>
                                </div>
                            </div>
                            <div class="single-communication">
                                <div class="communication-icon">
                                    <i class="ti-email" aria-hidden="true"></i>
                                </div>
                                <div class="communication-text">
                                    <h4>Email:</h4>
                                    <p><a href="#">contact@marianaciceu.com</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-message-wrapper">
                        <h4 class="contact-title">Contacteaza-ne</h4>
                        <div class="contact-message">

                            @if (session()->has('thank-you'))
                                <div class="alert alert-success">{{ session()->get('thank-you') }}</div>
                            @endif

                            {!! Form::open()->post()->route('contact.store') !!}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="contact-form-style mb-20">
                                            {!! Form::text('name', 'Nume')->placeholder('Nume complet') !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="contact-form-style mb-20">
                                            {!! Form::text('email', 'Mail')->placeholder('Adresa de mail')->type('email') !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="contact-form-style mb-20">
                                            {!! Form::text('subject', 'Subiect') !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="contact-form-style mb-20">
                                            {!! Form::textarea('message', 'Mesaj') !!}
                                        </div>
                                    </div>

                                    {!! Form::submit('Trimite') !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection