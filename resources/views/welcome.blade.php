@extends('adminlte::page')

@section('content')
    <div class="box">
        <div class="box-header">Useri</div>
        <div class="box-body">
            <table class="table table-responsive">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Actiuni</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>John</td>
                    <td>Test</td>
                    <td>
                        <a href="/test" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection