@extends('layouts.master')

@section('content')
	<div style="height:150px;"></div>
    <div class="about-us-area hm-3-padding pt-125 pb-125">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="about-us-details">
                         {!! $content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection