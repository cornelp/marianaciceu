<div class="box">
    <div class="box-header">
        {{ $buttons ?? '' }}
    </div>

    <div class="box-body">
        {{ $slot }}
    </div>
</div>