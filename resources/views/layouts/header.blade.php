<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Mariana Ciceu Couture</title>
        <meta name="description" content="Mariana Ciceu Couture">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png') }}">

		<!-- all css here -->
        <link rel="stylesheet" href="{{ asset('css/frontend.css') }}">

        @stack('css')

    </head>
    <body>
        <div class="wrapper">
            <!-- header start -->
            <header>
                <div class="header-area transparent-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-5 col-5">
                                 <!--<div class="language-currency">
                                    <div class="language">
                                        <select class="select-header orderby">
                                            <option value="">ENGLISH</option>
                                            <option value="">BANGLA </option>
                                            <option value="">HINDI</option>
                                        </select>
                                    </div>
                                    <div class="currency">
                                        <select class="select-header orderby">
                                            <option value="">USD</option>
                                            <option value="">US </option>
                                            <option value="">EURO</option>
                                        </select>
                                    </div>
                                </div>-->
                                <div class="sticky-logo">
                                    <a href="{{ url('/') }}"><img src="{{ asset('img/logo/logo_mc3.png') }}" alt="" /></a>
                                </div>
                                <div class="logo-small-device">
                                    <a href="{{ url('/') }}"><img alt="" src="{{ asset('img/logo/logo_mc2.png') }}"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 d-none d-md-block">
                                <div class="logo-menu-wrapper text-center">
                                    <div class="logo">
                                        <a href="{{ url('/') }}"><img src="{{ asset('img/logo/logo_mc.png') }}" alt="" /></a>
                                    </div>
                                    <div class="main-menu">
                                        <nav>
                                            <ul>
                                                <li><a href="{{ url('/') }}">acasa</a></li>
                                                <li><a href="{{ url('about') }}">despre mine </a></li>
                                                <li>
                                                    <a href="{{ route('collections.index') }}">Colectii <i class="ion-ios-arrow-down"></i></a>
                                                    <ul>
                                                        @foreach ($collections as $collection)
                                                            <li><a href="{{ route('collections.show', [$collection->id]) }}">{{ $collection->name }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
												<li><a href="{{ route('categories.index') }}">Categorii <i class="ion-ios-arrow-down"></i></a>
                                                    <ul>
                                                        @foreach ($categories as $category)
                                                            <li><a href="{{ route('categories.show', [$category->id]) }}">{{ $category->name }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                <li><a href="{{ url('contact') }}">contact</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-7 col-7">
                                <div class="header-site-icon">
                                    <div class="header-search same-style">
                                        <button class="sidebar-trigger-search">
                                            <span class="ti-search"></span>
                                        </button>
                                    </div>
                                    <!--<div class="header-login same-style">
                                        <a href="login-register.php">
                                            <span class="ti-user"></span>
                                        </a>
                                    </div>-->
                                    <div class="header-cart same-style">
                                        <button class="sidebar-trigger">
                                            <i class="ti-shopping-cart"></i>
                                            <span class="count-style">{{ $cart->count() }}</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-menu-area col-12">
                                <div class="mobile-menu">
                                    <nav id="mobile-menu-active">
                                        <ul class="menu-overflow">
                                            <li><a href="{{ url('/') }}">ACASA</a></li>
                                            <li><a href="{{ url('about') }}">Despre mine</a></li>
                                            <li><a href="#">Colectii</a>
                                                <ul>
                                                    @foreach ($collections as $collection)
                                                        <li><a href="{{ route('collections.show', [$collection->id]) }}">Colectii 1</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
											<li><a href="{{ route('categories.index') }}">Categorii</a>
                                                <ul>
                                                    @foreach ($categories as $category)
                                                        <li><a href="{{ route('categories.show', [$category->id]) }}">{{ $category->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            <li><a href="{{ url('contact') }}"> Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- sidebar-cart start -->
            <div class="sidebar-cart onepage-sidebar-area">
                @include('layouts.cart')
            </div>

            <!-- main-search start -->
            <div class="main-search-active">
                <div class="sidebar-search-icon">
                    <button class="search-close"><span class="ti-close"></span></button>
                </div>
                <div class="sidebar-search-input">
                    <form method="GET" action="{{ route('search') }}">
                        <div class="form-search">
                            <input id="search" name="search" class="input-text" value="" placeholder="Cauta" type="text">
                            <button>
                                <i class="ti-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
