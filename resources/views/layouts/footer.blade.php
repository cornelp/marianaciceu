           <footer class="gray-bg footer-padding text-center">
                <div class="container-fluid">
                    <div class="footer-top pt-85 pb-25">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-xs-6 col-sm-6">
                                <div class="footer-widget mb-30">
                                    <div class="footer-widget-title">
                                        <h3>Hai sa ne cunoastem</h3>
                                    </div>
                                    <div class="food-info-wrapper">
                                        <div class="food-address">
                                            <div class="food-info-icon">
                                                <i class="ion-ios-home-outline"></i>
                                            </div>
                                            <div class="food-info-content">
                                                <p>adresa exemplu</p>
                                            </div>
                                        </div>
                                        <div class="food-address">
                                            <div class="food-info-icon">
                                                <i class="ion-ios-telephone-outline"></i>
                                            </div>
                                            <div class="food-info-content">
                                                <p>(+00) 121 025 0214 </p>
                                            </div>
                                        </div>
                                        <div class="food-address">
                                            <div class="food-info-icon">
                                                <i class="ion-ios-email-outline"></i>
                                            </div>
                                            <div class="food-info-content">
                                                <p><a href="mailto: info@example.com">info@example.com</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-6 col-sm-6">
                                <div class="footer-widget mb-30">
                                    <div class="footer-widget-title">
                                        <h3>Informatii</h3>
                                    </div>
                                    <div class="food-widget-content">
                                        <ul class="quick-link">
                                            <li><a href="{{ url('delivery') }}">Livrare</a></li>
                                            <li><a href="{{ url('terms') }}">Termeni & Conditii</a></li>
                                            <li><a href="{{ url('about') }}">Despre Noi</a></li>
                                            <li><a href="http://www.anpc.gov.ro">ANPC</a></li>
                                            <li><a href="{{ url('contact') }}">Contact</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
                                <div class="footer-widget mb-30">
                                    <div class="footer-widget-title">
                                        <h3>Social Media</h3>
                                    </div>
                                    <div class="twitter-info-wrapper">
                                        <div class="single-twitter">
                                            <div class="twitter-icon">
                                                <i class="ion-social-facebook-outline"></i>
                                            </div>
                                            <div class="twitter-content">
                                                <a class="link2" href="https://www.facebook.com/MarianaCiceuCouture/">FACEBOOK</a></p>
                                            </div>
                                        </div>
                                        <div class="single-twitter">
                                            <div class="twitter-icon">
                                                <i class="ion-social-instagram-outline"></i>
                                            </div>
                                            <div class="twitter-content">
                                               <a class="link2" href="https://www.instagram.com/marianaciceucouture/">INSTAGRAM</a></p>
                                            </div>
                                        </div>
                                        <div class="single-twitter">
                                            <div class="twitter-icon">
                                                <i class="ion-social-pinterest-outline"></i>
                                            </div>
                                            <div class="twitter-content">
                                                <a class="link2" href="https://ro.pinterest.com/freshmarry">PINTEREST</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-bottom border-top-1 ptb-15">
                        <div class="row">
                            <div class="col-md-12 col-12 text-center">
                                <div class="copyright-payment">
                                    <div class="copyright">
                                        <p>created by <a href="http://bogdanbuganu.eu" target="_blank"> bogdanbuganu.eu</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="ion-android-close" aria-hidden="true"></span>
                </button>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {{-- @include('layouts.modal') --}}
                    </div>
                </div>
            </div>
        <!-- all js here -->
        @stack('js')
        <script src="{{ asset('js/modernizr-2.8.3.min.js') }}"></script>
        <script src="{{ asset('js/jquery-1.12.0.min.js') }}"></script>
        <script src="{{ asset('js/popper.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('js/waypoints.min.js') }}"></script>
        <script src="{{ asset('js/ajax-mail.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/plugins.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('js/frontend.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
        <script>
            let addToCart = function (event) {
                let data = {
                    color: $('#color').val(),
                    size: $('#size').val(),
                    cloth: $('#cloth').val(),
                    quantity: $('#quantity').val(),
                    product: $('#product').val(),
                };

                axios.post('/cart', data)
                    .then(function (response) {
                        $('.sidebar-cart').html(response.data);

                        $('.count-style').html($(response.data).find('.single-product-cart').length);

                        $.bootstrapGrowl('Produsul a fost adaugat in cos.', { type: 'success' });
                    });

                event.preventDefault();
            };

            let getQuickProduct = function (event, id) {
                axios.get('/quick-product/' + id)
                    .then(function (response) {
                        $('.modal-content').html(response.data);
                    });
            };
        </script>
    </body>
</html>
