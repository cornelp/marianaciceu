        <div class="wrap-sidebar">
            <div class="sidebar-cart-all">
                <div class="sidebar-cart-icon">
                    <button class="op-sidebar-close"><span class="ti-close"></span></button>
                </div>
                <div class="cart-content">
                    <h3>Cos de cumparaturi</h3>
                    @if ($cart->items())
                        <ul>
                            @foreach ($cart->items() as $item)
                                <li class="single-product-cart">
                                    <div class="cart-img">
                                        <a href="{{ route('products.show', [$item->product->id]) }}"><img src="{{ asset('images/' . $item->product->getFirstPicture()->name) }}" alt="{{ $item->product->getFirstPicture()->alt }}"></a>
                                    </div>
                                    <div class="cart-title">
                                        <h3><a href="{{ route('products.show', [$item->product->id]) }}">{{ $item->product->locale->pivot->name }}</a></h3>
                                        <span>{{ $item->quantity }} x @float($item->product->locale->pivot->price) {{ $item->product->locale->symbol }}</span>
                                    </div>
                                    <div class="cart-delete">
                                        <a href="#" onclick="deleteFromCart(event, '{{ $item->id }}')"><i class="ti-trash"></i></a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>

                        <div class="cart-checkout-btn">
                            <a class="cr-btn btn-style cart-btn-style" href="{{ route('cart.content') }}"><span>Vezi cos</span></a>
                        </div>
                    @else
                        Nu exista niciun element
                    @endif
                </div>
            </div>
        </div>

@push('js')
    <script>
        let deleteFromCart = function (event, id) {
            axios.delete('/cart/' + id)
                .then(function (response) {
                    $('.sidebar-cart').html(response.data);

                    $(document).find('.count-style').html($(response.data).find('.single-product-cart').length);

                    $.bootstrapGrowl('Produsul a fost eliminat din cos.', { type: 'info' });
                });
        };
    </script>
@endpush