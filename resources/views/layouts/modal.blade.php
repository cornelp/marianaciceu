<div class="modal-body">
    <div class="qwick-view-left">
        <div class="quick-view-learg-img">
            <div class="quick-view-tab-content tab-content">
                @foreach ($product->pictures as $index => $picture)
                    <div class="tab-pane fade @if ($loop->first) active show @endif" id="modal{{ $index }}" role="tabpanel">
                        <img src="{{ asset('images/' . $picture->name) }}" class="img-responsive" style="height: 380px; width:300px;" alt="{{ $picture->alt }}">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="quick-view-list nav" role="tablist">
            @foreach ($product->pictures as $index => $picture)
                <a @if ($loop->first) class="active" @endif href="#modal{{ $index }}" data-toggle="tab" role="tab" aria-selected="true" aria-controls="home{{ $index }}">
                    <img src="{{ asset('images/' . $picture->name) }}" style="height: 100px; width:112px;" alt="{{ $picture->alt }}">
                </a>
            @endforeach
        </div>
    </div>
    <div class="qwick-view-right">
        <div class="qwick-view-content">
            <h3>{{ $product->locale->pivot->name }}</h3>
            <input type="hidden" id="product" value="{{ $product->id }}">
            <div class="price">
                @if ($product->discount)
                    <span class="old">@float($product->locale->pivot->price) {{ $product->locale->symbol }}</span>
                @endif
                <span class="new">@float($product->getFinalPrice()) {{ $product->locale->symbol }}</span>
            </div>
            <p>{!! $product->locale->pivot->description !!}</p>
            <div class="quick-view-select">
                <div class="select-option-part">
                    <label>Marime*</label>
                    <select id="size">
                        @foreach ($product->sizes as $size)
                            <option value="{{ $size->id }}">{{ $size->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="select-option-part">
                    <label>Culoare*</label>
                    <select id="color">
                        @foreach ($product->colors as $color)
                            <option value="{{ $color->id }}">{{ $color->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="select-option-part">
                    <label>Material*</label>
                    <select id="cloth">
                        @foreach ($product->cloths as $cloth)
                            <option value="{{ $cloth->id }}">{{ $cloth->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="quickview-plus-minus">
                <div class="cart-plus-minus">
					<input type="text" value="1" id="quantity" name="qtybutton" class="cart-plus-minus-box">
				</div>
                <div class="quickview-btn-cart">
                    <a class="btn-style cr-btn" onclick="addToCart(event)" href="#"><span>adauga in cos</span></a>
                </div>
            </div>
        </div>
    </div>
</div>