@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            {!! Form::open()->post()->route('admin.first-page.store')->multipart() !!}
                {!! Form::file('picture', 'Imagine') !!}
                {!! Form::text('alt', 'Descriere poza') !!}
                {!! Form::text('title', 'Titlu') !!}
                {!! Form::textarea('text', 'Text') !!}
                {!! Form::submit('Salveaza') !!}
            {!! Form::close() !!}
        @endslot

        <div class="d-none">
            {!! Form::open()->delete()->route('admin.first-page.destroy', [0]) !!}
            {!! Form::close() !!}
        </div>

        @foreach($items as $item)
            <div class="card" style="width: 18rem;">
                <img class="card-img-top img-thumbnail" src="{{ asset('images/' . $item->picture->name) }}" alt="{{ $item->picture->alt }}">
                <div class="card-body">
                    <p class="card-text">{{ $item->text }}</p>
                    <a href="#" onclick="deletePicture(event, {{ $item->id }})" class="btn btn-danger">Sterge</a>
                </div>
            </div>
        @endforeach
    @endbox
@endsection

@push('js')
    <script>
        let deletePicture = function (event, id) {
            if (! confirm('Esti sigur(a)?')) {
                return false;
            }

            let form = $('.d-none form');

            form.attr('action', form.attr('action').replace(0, id));
            form.submit();

            event.preventDefault();
        };
    </script>
@endpush