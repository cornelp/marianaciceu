@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a class="btn btn-default" href="{{ route('admin.orders.details.index', $orderDetail->order_id) }}">Inapoi</a>
        @endslot

        {!! Form::open()->put()->route('admin.orders.details.update', [$orderDetail->order_id, $orderDetail->id])->fill($orderDetail) !!}
            {!! Form::select('product_id', $products, 'Produse') !!}
            {!! Form::text('quantity', 'Cantitate') !!}
            {!! Form::select('size_id', $sizes, 'Marime') !!}
            {!! Form::select('color_id', $colors, 'Culoare') !!}
            {!! Form::select('cloth_id', $cloths, 'Material') !!}
            {!! Form::text('price', 'Pret') !!}
            {!! Form::submit('Modifica') !!}
        {!! Form::close() !!}
    @endbox
@endsection