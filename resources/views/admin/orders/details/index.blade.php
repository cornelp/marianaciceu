@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a class="btn btn-default" href="{{ route('admin.orders.index') }}">Inapoi</a>
            <br>
            <br>
            Detalii pentru comanda {{ $order->id }}
        @endslot

        @include('admin.orders.edit', compact('order'))

        <h2>Detalii</h2>
        @if ($order->details->count())
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Nume produs</th>
                        <th>Marime</th>
                        <th>Culoare</th>
                        <th>Material</th>
                        <th>Cantitate</th>
                        <th>Pret</th>
                        <th>Actiuni</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($order->details as $detail)
                        <tr>
                            <td>{{ $detail->product->locale->pivot->name }}</td>
                            <td>{{ $detail->size->name }}</td>
                            <td>{{ $detail->color->name }}</td>
                            <td>{{ $detail->cloth->name }}</td>
                            <td>{{ $detail->quantity }}</td>
                            <td>@float($detail->price)</td>
                            <td>
                                <a href="{{ route('admin.orders.details.edit', [$detail->order_id, $detail->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    @endbox
@endsection