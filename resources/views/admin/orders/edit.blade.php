{!! Form::open()->put()->route('admin.orders.update', [$order->id])->fill($order) !!}
    {!! Form::text('name', 'Nume') !!}
    {!! Form::text('email', 'Email') !!}
    {!! Form::text('phone', 'Telefon') !!}
    {!! Form::text('city', 'Oras') !!}
    {!! Form::text('address', 'Adresa') !!}
    {!! Form::textarea('observations', 'Observatii') !!}
    {!! Form::select('is_processed', [0 => 'Nu', 1 => 'Da'], 'E procesata') !!}
    {!! Form::submit('Modifica') !!}
{!! Form::close() !!}