@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            {!! Form::open()->get()->route('admin.orders.index') !!}
                {!! Form::text('startDate', 'De la', $startDate->format('d.m.Y'))->params(['class' => 'datepicker']) !!}
                {!! Form::text('endDate', 'Pana la', $endDate->format('d.m.Y'))->params(['class' => 'datepicker']) !!}
                {!! Form::submit('Filtreaza') !!}
            {!! Form::close() !!}
        @endslot

        @if ($orders->count())
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nume</th>
                        <th>Email</th>
                        <th>Telefon</th>
                        <th>Oras</th>
                        <th>Adresa</th>
                        <th>Are observatii</th>
                        <th>E procesata</th>
                        <th>Creat la</th>
                        <th>Modificat la</th>
                        <th>Actiuni</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>{{ $order->city }}</td>
                            <td>{{ $order->address }}</td>
                            <td>{{ $order->observations ? 'Da' : 'Nu' }}</td>
                            <td>{{ $order->is_processed ? 'Da' : 'Nu' }}</td>
                            <td>{{ $order->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $order->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.orders.details.index', $order->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $orders->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker({ format: 'dd.mm.yyyy' });
        });
    </script>
@endpush