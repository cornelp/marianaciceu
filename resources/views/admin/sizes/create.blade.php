@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.sizes.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
        @endslot

        {!! Form::open()->route('admin.sizes.store') !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection