@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.sizes.create', 'name' => 'marime'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.sizes.index'])
                @endsearch
            </div>
        @endslot

        @if ($sizes->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'name' => 'Nume',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($sizes as $size)
                        <tr>
                            <td>{{ $size->id }}</td>
                            <td>{{ $size->name }}</td>
                            <td>{{ $size->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $size->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.sizes.edit', $size->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $sizes->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@section('js')
@endsection