@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza marime #{{ $size->id }}
        @endslot

        {!! Form::open()->put()->route('admin.sizes.update', [$size->id])->fill($size) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.sizes.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection