@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.products.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
        @endslot

        {!! Form::open()->route('admin.products.store') !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('category_id', $categories, 'Categorie') !!}
            {!! Form::select('collection_id', $collections, 'Colectie') !!}
            {!! Form::text('price', 'Pret')->placeholder('Lei') !!}
            {!! Form::textarea('description', 'Descriere') !!}
            {!! Form::select('is_new', [1 => 'Da', 0 => 'Nu'], 'E nou') !!}
            {!! Form::select('is_active', [1 => 'Da', 0 => 'Nu'], 'E activ') !!}
            {!! Form::text('discount', 'Discount')->placeholder('Procent (%)') !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-ro-RO.min.js"></script>
    <script>
        $(function () {
            $('#description').summernote();
        });
    </script>
@endpush