@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Adauga/sterge culori pentru produs #{{ $product->id }}
        @endslot

        {!! Form::open()->route('admin.products.colors.store', [$product->id]) !!}
            @foreach ($colors as $color)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" {{ in_array($color->id, $colorIds) ? 'checked' : '' }} value="{{ $color->id }}" name="color[]">
                    <label class="form-check-label" for="defaultCheck1">{{ $color->name }}</label>
                </div>
            @endforeach

            {!! Form::submit('Salveaza')->success() !!}
            {!! Form::anchor('Inapoi')->route('admin.products.index') !!}
        {!! Form::close() !!}
    @endbox
@endsection