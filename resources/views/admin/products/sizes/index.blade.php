@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Adauga/sterge marimi pentru produs #{{ $product->id }}
        @endslot

        {!! Form::open()->route('admin.products.sizes.store', [$product->id]) !!}
            @foreach ($sizes as $size)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" {{ in_array($size->id, $sizeIds) ? 'checked' : '' }} value="{{ $size->id }}" name="size[]">
                    <label class="form-check-label" for="defaultCheck1">{{ $size->name }}</label>
                </div>
            @endforeach

            {!! Form::submit('Salveaza')->success() !!}
            {!! Form::anchor('Inapoi')->route('admin.products.index') !!}
        {!! Form::close() !!}
    @endbox
@endsection