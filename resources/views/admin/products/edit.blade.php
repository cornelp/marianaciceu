@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza produs #{{ $data['id'] }}
        @endslot

        {!! Form::open()->put()->route('admin.products.update', [$data['id']])->fill($data) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('category_id', $categories, 'Categorie') !!}
            {!! Form::select('collection_id', $collections, 'Colectii') !!}
            {!! Form::text('price', 'Pret') !!}
            {!! Form::textarea('description', 'Descriere') !!}
            {!! Form::select('is_new', [0 => 'Nu', 1 => 'Da'], 'E nou') !!}
            {!! Form::select('is_active', [0 => 'Nu', 1 => 'Da'], 'E activ') !!}
            {!! Form::text('discount', 'Discount (%)')->type('number') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.products.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-ro-RO.min.js"></script>
    <script>
        $(function () {
            $('#description').summernote();
        });
    </script>
@endpush