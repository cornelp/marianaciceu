@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Adauga/sterge materiale pentru produs #{{ $product->id }}
        @endslot

        {!! Form::open()->route('admin.products.cloths.store', [$product->id]) !!}
            @foreach ($cloths as $cloth)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" {{ in_array($cloth->id, $clothIds) ? 'checked' : '' }} value="{{ $cloth->id }}" name="cloth[]">
                    <label class="form-check-label" for="defaultCheck1">{{ $cloth->name }}</label>
                </div>
            @endforeach

            {!! Form::submit('Salveaza')->success() !!}
            {!! Form::anchor('Inapoi')->route('admin.products.index') !!}
        {!! Form::close() !!}
    @endbox
@endsection