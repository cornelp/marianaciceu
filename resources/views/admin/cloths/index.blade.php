@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.cloths.create', 'name' => 'material'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.cloths.index'])
                @endsearch
            </div>
        @endslot

        @if ($cloths->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'name' => 'Nume',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($cloths as $cloth)
                        <tr>
                            <td>{{ $cloth->id }}</td>
                            <td>{{ $cloth->name }}</td>
                            <td>{{ $cloth->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $cloth->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.cloths.edit', $cloth->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $cloths->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@section('js')
@endsection