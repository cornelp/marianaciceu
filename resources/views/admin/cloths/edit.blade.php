@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza culoare #{{ $cloth->id }}
        @endslot

        {!! Form::open()->put()->route('admin.cloths.update', [$cloth->id])->fill($cloth) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.cloths.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection