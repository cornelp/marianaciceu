@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza colectie #{{ $collection->id }}
        @endslot

        {!! Form::open()->put()->route('admin.collections.update', [$collection->id])->fill($collection) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.collections.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection