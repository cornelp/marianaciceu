@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza culoare #{{ $color->id }}
        @endslot

        {!! Form::open()->put()->route('admin.colors.update', [$color->id])->fill($color) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.products.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection