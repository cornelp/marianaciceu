@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.colors.create', 'name' => 'culoare'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.sizes.index'])
                @endsearch
            </div>
        @endslot

        @if ($colors->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'name' => 'Nume',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($colors as $color)
                        <tr>
                            <td>{{ $color->id }}</td>
                            <td>{{ $color->name }}</td>
                            <td>{{ $color->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $color->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.colors.edit', $color->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $colors->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@section('js')
@endsection