@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza culoare #{{ $category->id }}
        @endslot

        {!! Form::open()->put()->route('admin.categories.update', [$category->id])->fill($category) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('is_active', [0 => 'Nu', 1 => 'Da'], 'E activ') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.categories.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection