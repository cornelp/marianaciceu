@extends('layouts.master')

@section('content')
<div style="height:150px;"></div>
<div class="shop-wrapper hm-3-padding pt-120">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="product-filter-wrapper">
                    <div class="row">
                        <div class="product-filter col-md-3 col-sm-6 col-xs-12 mb-30">
                            <h5>Sort by</h5>
                        </div>
                        <div class="product-filter col-md-3 col-sm-6 col-xs-12 mb-30">
                            <h5>Culoare</h5>
                            <ul class="color-filter">
                                @foreach ($colors as $color)
                                    <li><a href="#"><i style="background-color: #000000;"></i>{{ $color->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="product-filter col-md-3 col-sm-6 col-xs-12 mb-30">
                            <h5>Marime</h5>
                            <ul class="color-filter">
                                @foreach ($sizes as $size)
                                    <li><a href="#"><i></i>{{ $size->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-list-product-wrapper">
            <div class="product-grid product-view">
                <div class="row">

                    @foreach ($products as $product)
                        <div class="product-width col-md-6 col-xl-3 col-lg-4 col-6">
                            <div class="product-wrapper mb-35">
                                <div class="product-img">
                                    <a href="{{ route('products.show', [$product->id]) }}">
                                        <img src="{{ asset('images/' . $product->getFirstPicture()->name) }}" alt="{{ $product->getFirstPicture()->alt }}">
                                    </a>
                                    @if ($product->discount)
                                        <div class="price-decrease">
                                            <span>{{ $product->getDiscount() }}% off</span>
                                        </div>
                                    @endif
                                    <div class="product-action-3">
                                        <a class="action-plus-2" title="Quick View" data-toggle="modal" onclick="getQuickProduct(event, {{ $product->id }})" data-target="#exampleModal" href="#">
                                            <i class="ti-plus"></i> Vizualizare rapida
                                        </a>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title-wishlist">
                                        <div class="product-title-3">
                                            <h4><a href="{{ route('products.show', [$product->id]) }}">{{ $product->locale->pivot->name }}</a></h4>
                                        </div>
                                    </div>
                                    <div class="product-peice-addtocart">
                                        <div class="product-peice-3">
                                            @if ($product->discount)
                                                <span class="old">@float($product->locale->pivot->price) {{ $product->locale->symbol }}</span>
                                            @endif
                                            <span style="font-size:16px;"><b>@float($product->getFinalPrice())</b> {{ $product->locale->symbol }}</span>
                                        </div>
                                        <div class="product-addtocart centre">
                                            <a href="#" style="font-size:20px;"><i class="ti-shopping-cart"></i>Adauga in cos</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

            <div class="product-width col-md-6 col-xl-3 col-lg-4 col-6"
                {{ $products->links() }}
            </div>
        </div>
        </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
    </script>
@endpush