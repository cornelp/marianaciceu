@extends('layouts.master')

@section('content')
    <div style="height:150px;"></div>
    <div class="product-details-area hm-3-padding ptb-130">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-details-img-content">
                        <div class="product-details-tab mr-40">
                            <div class="product-details-large tab-content">
                                @foreach ($product->pictures as $index => $picture)
                                    <div class="tab-pane @if ($loop->first) active @endif" id="pro-details{{ $index }}">
                                        <div class="easyzoom easyzoom--overlay">
                                            <a href="{{ asset('images/' . $picture->name) }}">
                                                <img src="{{ asset('images/' . $picture->name) }}" alt="{{ $picture->alt }}">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="product-details-small nav mt-12 product-dec-slider owl-carousel">
                                @foreach ($product->pictures as $index => $picture)
                                    <a @if ($loop->first) class="active" @endif href="#pro-details{{ $index }}">
                                        <img src="{{ asset('images/' . $picture->name) }}" alt="{{ $picture->alt }}">
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product-details-content">
                        <h2>{{ $product->locale->pivot->name }}</h2>
                        <div class="product-price">
                            @if ($product->discount)
                                <span class="old">@float($product->locale->pivot->price) {{ $product->locale->symbol }}</span>
                            @endif
                            <span>@float($product->getFinalPrice()) {{ $product->locale->symbol }}</span>
                        </div>
                        <div class="product-overview">
                            <h5 class="pd-sub-title">Detalii Produs</h5>
                            <p>{!! $product->locale->pivot->description !!}</p>
                        </div>
                        <div class="product-cloth">
                            <h5 class="pd-sub-title">Material</h5>
                            <select name="cloth" id="cloth">
                                @foreach ($product->cloths as $cloth)
                                    <option value="{{ $cloth->id }}">{{ $cloth->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="product-size">
                            <h5 class="pd-sub-title">Marime </h5><a href="#success" data-toggle="modal"><i class="ti-ruler-alt"></i>ghid</a>
                            <select name="size" id="size">
                                @foreach ($product->sizes as $size)
                                    <option value="{{ $size->id }}">{{ $size->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="product-color">
                            <h5 class="pd-sub-title">Culoare</h5>
                            <select name="color" id="color">
                                @foreach ($product->colors as $color)
                                    <option value="{{ $color->id }}">{{ $color->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" id="product" value="{{ $product->id }}">
                        <div class="quickview-plus-minus">
                            <div class="cart-plus-minus">
                                <input type="text" id="quantity" value="1" name="qtybutton" class="cart-plus-minus-box">
                            </div>
                            <div class="quickview-btn-cart">
                                <a class="btn-style cr-btn" onclick="addToCart(event)" href="#"><span>Adauga in cos</span></a>
                            </div>
                        </div>
                        <div class="product-share">
                            <h5 class="pd-sub-title">Share to Facebook</h5>
                            <ul>
                                <div class="fb-share-button" data-href="{{ route('products.show', [$product->id]) }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ route('products.show', [$product->id]) }}2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><i class="ion-social-facebook" style="font-size:38px;"></i></a></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1><i class="glyphicon glyphicon-thumbs-up"></i> </h1>
                </div>
                <div class="modal-body">
                          <table class="table text-center">
                          <thead>
                            <tr>
                              <th scope="col">MARIME</th>
                              <th scope="col">BUST <br> (cm)</th>
                              <th scope="col">TALIE <br> (cm)</th>
                              <th scope="col">SOLD <br> (cm)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">34</th>
                              <td>80</td>
                              <td>60</td>
                              <td>88</td>
                            </tr>
                            <tr>
                              <th scope="row">36</th>
                              <td>84</td>
                              <td>64</td>
                              <td>97</td>
                            </tr>
                            <tr>
                              <th scope="row">38</th>
                              <td>88</td>
                              <td>68</td>
                              <td>96</td>
                            </tr>
                            <tr>
                              <th scope="row">40</th>
                              <td>92</td>
                              <td>72</td>
                              <td>100</td>
                            </tr>
                            <tr>
                              <th scope="row">42</th>
                              <td>96</td>
                              <td>76</td>
                              <td>104</td>
                            </tr>
                          </tbody>
                        </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Inchide</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@push('js')
    <script>
        let addToCart = function (event) {
            let data = {
                color: $('#color').val(),
                size: $('#size').val(),
                cloth: $('#cloth').val(),
                quantity: $('#quantity').val(),
                product: $('#product').val(),
            };

            axios.post('/cart', data)
                .then(function (response) {
                    $('.sidebar-cart').html(response.data);

                    $('.count-style').html($(response.data).find('.single-product-cart').length);

                    $.bootstrapGrowl('Produsul a fost adaugat in cos.', { type: 'success' });
                });

            event.preventDefault();
        };
    </script>
@endpush