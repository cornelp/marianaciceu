@extends('layouts.master')

@section('content')
<div style="height:150px;"></div>
        <div class="product-cart-area hm-3-padding pt-120 pb-130">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="table-content table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product-name">Produs</th>
                                        <th class="product-price">Nume produs</th>
                                        <th class="product-name">Pret</th>
                                        <th class="product-price">Cantitate</th>
                                        <th class="product-quantity">Total</th>
                                        <th class="product-subtotal">Sterge</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form class="form-items" action="{{ route('cart.content.update') }}" method="POST">
                                        @method('PUT')
                                        @csrf
                                    @foreach ($cart->items() as $item)
                                        <tr>
                                            <td class="product-thumbnail">
                                                <a href="{{ route('products.show', [$item->product->id]) }}"><img src="{{ asset('images/' . $item->product->getFirstPicture()->name) }}" alt="{{ $item->product->getFirstPicture()->alt }}"></a>
                                            </td>
                                            <td class="product-name">
                                                <a href="{{ route('products.show', [$item->product->id]) }}">{{ $item->product->locale->pivot->name }}</a>
                                            </td>
                                            <td class="product-price"><span class="amount">@float($item->product->getFinalPrice()) {{ $item->product->locale->symbol }}</span></td>
                                            <td class="product-quantity">
                                                <div class="quantity-range">
                                                    <input class="input-text qty text" name="items[{{ $item->id }}]" type="number" step="1" min="0" value="{{ $item->quantity }}" title="Qty" size="4">
                                                </div>
                                            </td>
                                            <td class="product-subtotal">@float($item->product->getFinalPrice() * $item->quantity) {{ $item->product->locale->symbol }}</td>
                                            <td class="product-cart-icon product-subtotal"><a onclick="refreshView(event, '{{ $item->id }}')" href="#"><i class="ion-ios-trash-outline" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--<div class="col-md-7 col-sm-6">
                        <div class="discount-code">
                            <h4>enter your discount code</h4>
                            <div class="coupon">
                                <input type="text">
                                <input class="cart-submit" type="submit" value="enter">
                            </div>
                        </div>
                    </div>-->
                    <div class="col-md-12 col-sm-12">
                        <div class="shop-total">
                            <h3>Cos de cumparaturi</h3>
                            <ul>
                                <li>
                                    sub total
                                    <span>@float($cart->getTotal()) RON</span>
                                </li>
                                <li class="order-total">
                                    Transport
                                    <span>0</span>
                                </li>
                                <li>
                                    TOTAL
                                    <span>@float($cart->getTotal() + 0) RON</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="cart-shiping-update">
                            <div class="update-cart">
                                <a class="btn-style cr-btn" href="{{ url('/') }}">
                                    <span style="font-size:12px;">continua cumparaturile</span>
                                </a>
                            </div>
                            <div class="update-checkout-cart">
                                <div class="update-cart">
                                    <button type="submit" class="btn-style cr-btn"><span style="font-size:12px;">actualizeaza cos</span></button>
                                </div>
                            </form>
                                <div class="update-cart">
                                    <a class="btn-style cr-btn" href="{{ route('checkout.index') }}">
                                        <span style="font-size:12px;">Finalizeaza comanda</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <b>*Transportul</b> este gratuit. In cazul unui retur clientul este cel care suporta costurile de transport.
                </div>
            </div>
        </div>
@endsection

@push('js')
    <script>
        let refreshView = function (event, id) {
            deleteFromCart(event, id);

            setTimeout(function () {
                location.reload();
            }, 200);
        };
    </script>
@endpush