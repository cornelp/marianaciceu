@extends('layouts.master')

@section('content')
<div style="height:150px;"></div>
            <!-- checkout-area start -->
            <div class="checkout-area pt-130 hm-3-padding pb-100">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12">
                            <form action="{{ route('checkout.store') }}" method="POST">
                                @csrf
                                <div class="checkbox-form">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <h3>Detalii Livrare/Facturare</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Nume<span class="required">*</span></label>
                                                <input type="text" name="name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Adresa <span class="required">*</span></label>
                                                <input type="text" name="address" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Telefon<span class="required">*</span></label>
                                                <input type="text" name="phone" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Oras <span class="required">*</span></label>
                                                <input type="text" name="city" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Email <span class="required">*</span></label>
                                                <input type="email" name="email" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="different-address">
                                        <div class="order-notes">
                                            <div class="checkout-form-list mrg-nn">
                                                <label>Mentiuni</label>
                                                <textarea id="checkout-mess" name="observations" cols="30" rows="10" placeholder="Daca aveti cerinte in plus despre comanda dumneavoastra va rog sa le scrieti aici." ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="terms" required>Sunt de acord cu <a target="_blank" href="{{ url('/terms') }}">termenii si conditiile.</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="your-order">
                                <h3>Detalii despre comanda</h3>
                                <div class="your-order-table table-responsive">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th class="product-name">Produs</th>
                                                <th class="product-total">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cart->items() as $item)
                                                <tr class="cart_item">
                                                    <td class="product-name">
                                                        {{ $item->product->locale->pivot->name }} <strong class="product-quantity"> × {{ $item->quantity }}</strong>
                                                    </td>
                                                    <td class="product-total">
                                                    <span class="amount">@float($item->product->getFinalPrice()) {{ $item->product->locale->symbol }}</span>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr class="cart-subtotal">
                                            <th>Subtotal</th>
                                            <td><span class="amount">@float($cart->getTotal()) RON</span></td>
                                        </tr>
                                        <tr class="order-total">
                                            <th>Total</th>
                                            <td><strong><span class="amount">@float($cart->getTotal()) RON</span></strong>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="payment-method mt-40">
                                <div class="payment-accordion">
                                    <div class="panel-group" id="faq">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5 class="panel-title"><a data-toggle="collapse" aria-expanded="true" data-parent="#faq" href="#payment-1">Modalitati de plata</a></h5>
                                            </div>
                                            <div id="payment-1" class="panel-collapse collapse ">
                                                <div class="panel-body">
                                                    <p>Online cu card bancar (Visa Classic/Electron, Mastercard, Maestro)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-group" id="livrare">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5 class="panel-title"><a data-toggle="collapse" aria-expanded="false" data-parent="#livrare" href="#livrare-1">Livrare</a></h5>
                                            </div>
                                            <div id="livrare-1" class="panel-collapse collapse ">
                                                <div class="panel-body">
                                                    <p>Transportul este gratuit.<br>In cazul in care doriti sa faceti retur, costul transportului va fi suportat de dumneavoastra.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-button-payment">
                                        <input type="submit" value="Plaseaza comanda" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
@endsection