<?php

return [

    'full_name'                   => 'Nume Complet',
    'email'                       => 'Email',
    'password'                    => 'Parola',
    'retype_password'             => 'Rescrie parola',
    'remember_me'                 => 'Tine-ma minte',
    'register'                    => 'Inregistrare',
    'register_a_new_membership'   => 'Register a new membership',
    'i_forgot_my_password'        => 'Mi-am uitat parola',
    'i_already_have_a_membership' => 'I already have a membership',
    'sign_in'                     => 'Sign In',
    'log_out'                     => 'Log Out',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Logheaza-te pentru a incepe sesiunea',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reseteaza parola',
    'reset_password'              => 'Reseteaza parola',
    'send_password_reset_link'    => 'Trimite link de resetare',
];
